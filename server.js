var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./MOCK_DATA.json');
var URLbase = "/colapi/v1/";

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());

// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);
});

// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log("GET /colapi/v2/users/:id/:otro");
   console.log(req.params);
   console.log('req.params.id: ' + req.params.id);
   console.log('req.params.otro: ' + req.params.otro);
   var respuesta = req.params;
   res.send(respuesta);
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
app.post(URLbase + 'users',
 function(req, res) {
   var newID = usersFile.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "country" : req.body.country
   };
   usersFile.push(newUser);
   console.log(usersFile);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });

 // PUT
 app.put(URLbase + 'users/:id',
   function(req, res){
     console.log("PUT /colapi/v2/users/:id");
     var idBuscar = req.params.id;
     var updateUser = req.body;  // para acceder al body del json que es la informacion con la que se pisara lo actual
     var encontrado = false;
     for(i = 0; (i < usersFile.length) && !encontrado; i++) {
       console.log(usersFile[i].id);
       if(usersFile[i].id == idBuscar) {
         encontrado = true;
         res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
       }
     }
     if (!encontrado){
        res.send({"msg" : "404 Usuario no encontrado.", updateUser});
     }
});

// DELETE ... aqui no se requiere informacion del body... no lo usa porque llega en la url el id a borrar y ya
app.delete(URLbase + 'users/:id',
  function(req, res){
    console.log("DELETE /colapi/v2/users/:id");
    var idBorrar = req.params.id;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBorrar) {
        res.send({"msg" : "204 Usuario eliminado coreectamente.", idBorrar});
        usersFile.splice(idBorrar-1,1) // elimino de la colleccion de memoria (aun no de la bd) el registro cuyo indice sea id-1
                                       // asumiendo que el registro de id 3 esta en la posicion 2 (0,1,2)
      }
    }
    res.send({"msg" : "404 Usuario no encontrado.", idBorrar});
  });

// el codigo 202 se usa en batch que no requiera la presencia del usuario
