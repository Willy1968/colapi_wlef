# Imagen inicial a partir de la cual creamos nuestra imagen
FROM node

# Definimos carpeta del contenedor
WORKDIR /colapi_wlef

# Adicionamos contenido del proyecto al directorio del contenedor
ADD . /colapi_wlef

# Puerto en el que escuchara el contenedor, el que ya hayamos definido
# en el package.json
EXPOSE 3000

# Comandos para lanzar nuestra API REST 'colapi'
CMD ["npm","start"]

#cuando ya no usemos nodemon
#CMD ["node","server_v3.js"]
