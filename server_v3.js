//********************************************************************
// server_v3.js
// PROYECTO TECH-U PRATITIONER - BBVA Colombia
// CONSUMO DE APIS A COLECCIONES MongoDB A TRAVES DE LA API mLab
// WILLIAM LEONARDO ESPITIA FRANCO
//********************************************************************
var express = require('express');  // inclusión (carga) del framework Express
var app = express();               // variable asociada a la funcion Express

var bodyParser = require('body-parser'); // inclusión de la utilidad para tomar
                                         //los datos del body de la peticion

var requestJson = require('request-json'); //

var port = process.env.PORT || 3000; // se establece el puerto de comunicación

var URLbase = "/colapi/v3/";

// base de la URL para acceder via mLab a MongoDB y apiKey
var urlbaseMlab = "https://api.mlab.com/api/1/databases/colapidb_wlef/collections/";
var apiKey = "apiKey=2ftt8atx8dqIgw6Q8EPfIjP4LF74m2m0";

// poner en modo escucha al servidor y definición de funcion callback la cual se ejecutará
// (escuchando por el puerto indicado) cuando el servidor este listo
app.listen(port, function()
  {console.log("API /colapi/V3 escuchando en el puerto " + port + "...");
  });

app.use(bodyParser.json()); // indicar al sistema que se quiere utilizat json


//*******************************************************************
//                                USER
//*******************************************************************

// GET user a traves de mLab para retornar el LISTADO COMPLETO de registros de la coleccion user
// ejemplo de llamado en Postman via GET localhost:3000/colapi/v3/user
app.get(URLbase + 'user',
 function(req, res) {
   console.log("GET /colapi/V3/user");
   var httpClient = requestJson.createClient(urlbaseMlab); //instanciar para poder hacer las peticiones
   var queryString = 'f={"_id":0}&'; // para no retornar el id propio de mLab
   httpClient.get('user?'+ queryString + apiKey, // accedemos a la coleccion user(recurso)
       function(err,respuestaMlab,body){  // err si hay error al acceder a mLab, err quedaría en true
                                         // respuestaMlab es donde se recibira toda la respuesta de mLab
             console.log("error = " + err);
             console.log("respuestaMlab = " + respuestaMlab);
             console.log("body = " + body);
             var respuesta = {};
             respuesta = !err ? body : {"msg": "Error al recuperar user mLab"}; //si Ok solo retornamos el body
             res.send(respuesta);
       });
});

// GET user a traves de mLab para retornar un REGISTRO ESPECIFICO de la coleccion user
// ejemplo de llamado en Postman via GET localhost:3000/colapi/v3/user/2
app.get(URLbase + 'user/:id',
 function (req, res) {
   console.log("GET /colapi/v3/user/:id");
   console.log("req.params.id=" + req.params.id);

   var id = req.params.id; // el id que queremos retornar
   var queryString = 'f={"_id":0}&'+'q={"id":' + id +'}&';  // se le puede adicionar 'f={"_id":0}&'
   var httpClient = requestJson.createClient(urlbaseMlab);

   httpClient.get('user?' + queryString + apiKey,
   function(err,respuestaMlab,body){
         console.log("respuesta mLab correcta" );
         console.log("body = " + body);
         var respuesta = body[0]; // el primer registro que encuentre
         res.send(respuesta);
   });
});


//*******************************************************************
//                                ACCOUNT
//*******************************************************************

// GET account a traves de mLab para retornar el LISTADO COMPLETO de registros de la coleccion account
// ejemplo de llamado en Postman via GET localhost:3000/colapi/v3/account
app.get(URLbase + 'account',
 function(req, res) {
   console.log("GET /colapi/V3/account");

   var httpClient = requestJson.createClient(urlbaseMlab);
   var queryString = 'f={"_id":0}&';

   httpClient.get('account?'+ queryString + apiKey,
       function(err,respuestaMlab,body){
             console.log("error = " + err);
             console.log("error = " + respuestaMlab);
             console.log("body = " +  body);
             var respuesta = {};
             respuesta = !err ? body : {"msg": "Error al recuperar account mLab"};
             res.send(respuesta);
       });
});

// GET account a traves de mLab para retornar REGISTROS ESPECIFICOS de la coleccion account, por user_id
// ejemplo de llamado en Postman via GET localhost:3000/colapi/v3/user/4/account
app.get(URLbase + 'user/:user_id/account/',
 function (req, res) {
   console.log("GET /colapi/v3/user/:user_id/account/"); // siguiendo el estándar
   console.log("user_id = " + req.params.user_id);

   var id = req.params.user_id;
   var queryString = 'f={"_id":0}&'+'q={"user_id":' + id +'}&';

   var httpClient = requestJson.createClient(urlbaseMlab);
   httpClient.get('account?' + queryString + apiKey,
   function(err,respuestaMlab,body){
         console.log("respuesta mLab account correcta" );
         console.log("body = " + body);
         var respuesta = {};
         respuesta = !err ? body : {"msg": "Error al recuperar account mLab"}; //solo retornamos el body

         res.send(respuesta);
   });
});


//*******************************************************************
//                                MOVEMENT
//*******************************************************************
// GET movement a traves de mLab para retornar el LISTADO COMPLETO de registros de la coleccion movement
// ejemplo de llamado en Postman via GET localhost:3000/colapi/v3/movement
app.get(URLbase+'movement',
 function(req, res) {
   console.log("GET /colapi/V3/movement");
   var httpClient = requestJson.createClient(urlbaseMlab);
   var queryString = 'f={"_id":0}&';
   httpClient.get('movement?'+queryString + apiKey,
       function(err,respuestaMlab,body){
             console.log("error = " + err);
             console.log("error = " + respuestaMlab);
             console.log("body = " +  body);
             var respuesta = {};
             respuesta = !err ? body : {"msg": "Error al recuperar movement mLab"}; //solo retornamos el body
             res.send(respuesta);
       });
});


// GET movement para retornar los movimientos de una cuenta a traves de mLab siguiendo estandar en URL
// ejemplo de llamado en Postman via GET localhost:3000/colapi/v3/user/1/account/"MK77 795S CRU7 IXWA E71"/movement
app.get(URLbase + 'user/:id/account/:IBAN/movement',
 function (req, res) {
   console.log("GET /colapi/v3/user/:id/account/:IBAN/movement");
   console.log("req.params.IBAN= "+ req.params.IBAN);

   var cuenta = req.params.IBAN;
   var queryString = 'f={"_id":0}&'+'q={"IBAN":' + cuenta +'}&';
   var httpClient = requestJson.createClient(urlbaseMlab);
   httpClient.get('movement?' + queryString + apiKey,
   function(err,respuestaMlab,body){
         console.log("respuesta mlab movement correcta" );
         console.log("body = " + body);
         var respuesta = {};
         respuesta = !err ? body : {"msg": "Error al recuperar movement mLab"}; //solo retornamos el body

         res.send(respuesta);
   });
});


//*****************************************************************************************************
//                                      POST Y PUT DE USER
//*****************************************************************************************************

// POST de un usuario utilizando mlab
// insertar un nuevo usuario.
// ejemplo de llamado en Postman via POST localhost:3000/colapi/v3/user y en el body enviar la informacion
app.post(URLbase + 'user',
 function(req, res) {
   clienteMlab = requestJson.createClient(urlbaseMlab + "/user?" + apiKey)
   clienteMlab.post('', req.body,
     function(err, resM, body) {
         res.send(body)
     });
});


// PUT de un usuario utilizando mlab
// actualizar un usuario que ya existe
// ejemplo de llamado en Postman via PUT localhost:3000/colapi/v3/user/2 y en el body enviar la informacion
app.put(URLbase + 'user/:id', function(req, res) {
 clienteMlab = requestJson.createClient(urlbaseMlab  + "/user")
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}' /*en mlab toca con $set siempre para hacer una modificacion*/

 clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio),
  function(err, resM, body) {
   res.send(body) /* en el put le pasamos lo que hemos recibido en el body o sea el json modificado */
 });
});

//*****************************************************************************************************
//                                      LOGIN CON mLab
//*****************************************************************************************************
// ejemplo de llamado en Postman via POST localhost:3000/colapi/v3/login
app.post(URLbase + 'login',
 function(req, res) {
   clienteMlab = requestJson.createClient(urlbaseMlab)
   var mailrec = req.body.email;
   var claverec = req.body.password;
   var encontrado = false;
   var queryString = 'q={"email":' + '"' + mailrec + '"' + '}&';
   var httpClient = requestJson.createClient(urlbaseMlab);
   httpClient.get('user?' + queryString + apiKey,
      function(err,respuestaMlab,body){
            console.log("queryString = " + queryString);
            console.log ("email= " + mailrec);
            console.log("respuesta mLab get DE VALIDACION correcta" );
            var respuesta = {};
            console.log("body = " + JSON.stringify(req.body));
            if (body.length == 0)
            {
              console.log("usuario NO existe= " + req.body.email);
            }
            else
            {
              console.log("usuario SI existe= " + req.body.email);
              console.log("body.password = " + body[0].password);
              console.log("claverec = " + claverec);
              if (body[0].password == claverec)
              {
                console.log("password correcto para el id= " + body[0].id);

                clienteMlabP = requestJson.createClient(urlbaseMlab  + "/user");

                var logged = {"logged": true};
                var cambio = '{"$set":' + JSON.stringify(logged) + '}';

                clienteMlabP.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio),
                 function(err, resM, body) {
                  res.send("OK");
                });
              }
              else
              {
                console.log("password INVALIDO para el id= " + body[0].id);
              }
            }
        });
   });

   //*****************************************************************************************************
   //                                      LOGOUT CON mLab
   //*****************************************************************************************************
   // ejemplo de llamado en Postman via POST localhost:3000/colapi/v3/logout
   app.post(URLbase + 'logout',
    function(req, res) {
      clienteMlab = requestJson.createClient(urlbaseMlab)
      var mailrec = req.body.email;
      var encontrado = false;
      var queryString = 'q={"email":' + '"' + mailrec + '"' + '}&';
      var httpClient = requestJson.createClient(urlbaseMlab);
      httpClient.get('user?' + queryString + apiKey,
         function(err,respuestaMlab,body){
               console.log("queryString = " + queryString);
               console.log ("email= " + mailrec);
               console.log("respuesta mLab get DE VALIDACION correcta" );
               var respuesta = {};
               console.log("body = " + JSON.stringify(req.body));
               if (body.length == 0)
               {
                 console.log("usuario NO existe= " + req.body.email);
               }
               else
               {
                 console.log("usuario SI existe= " + req.body.email);

                   clienteMlabP = requestJson.createClient(urlbaseMlab  + "/user");

                   var logged = {"logged": true};
                   var cambio = '{"$unset":' + JSON.stringify(logged) + '}';

                   clienteMlabP.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio),
                    function(err, resM, body) {
                     res.send("OK. Usuario deslogueado");
                   });
                 }

               }
           );
      });

//********************************************************************
// FIN PROYECTO TECH-U PRATITIONER - BBVA Colombia
//********************************************************************
