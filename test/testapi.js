var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server_v3');

var should = chai.should();

chai.use(chaiHttp) //Configurar chai con modulo HTTP

describe('Pruebas Colombia server_v3.js', () =>{
  it('BBVA funciona',(done) =>  {
     chai.request('http://www.bbva.com')
     .get('/')
     .end( (err,res) => {
    //   console.log(res)
    //   res.should.have.status(200)
       done();
     })
  });

  it('Mi API funciona',(done) =>  {
     chai.request('http://localhost:3000')
     .get('/colapi/v3/user')
     .end( (err,res) => {
       //console.log(res.body)
       res.should.have.status(200)
       done();
     })
  });

  it('Mi API trae los USER en un array',(done) =>  {
     chai.request('http://localhost:3000')
     .get('/colapi/v3/user')
     .end( (err,res) => {
       //console.log(res.body)
       res.body.should.be.a('array')
       done();
     })
  });

  it('Mi API2 trae los USER en un array mayor a',(done) =>  {
     chai.request('http://localhost:3000')
     .get('/colapi/v3/user')
     .end( (err,res) => {
       //console.log(res.body)
       res.body.length.should.be.gte(1)
       done();
     })
  });

  it('Mi API2 trae los USER en un array mayor a',(done) =>  {
     chai.request('http://localhost:3000')
     .get('/colapi/v3/user/')
     .end( (err,res) => {
       //console.log(res.body[0])
       res.body[0].should.have.property('first_name')
       res.body[0].should.have.property('last_name')
       done();
     })
  });


  it('Mi API trae un usuario especìfico',(done) =>  {
     chai.request('http://localhost:3000')
     .get('/colapi/v3/user/4')
     .end( (err,res) => {
       //console.log(res)
       res.should.have.status(200)
       done();
     })
  });

  it('Validar crear elemento',(done) =>  {
     chai.request('http://localhost:3000')
     .post('/colapi/v3/user')
     .send('{"id":"2633038","first_name":"Sandra","last_name":"Bocanegra"}')
     .end( (err,res,body) => {
       //console.log(res)
       body.should.be.eql('lo que responda el post')
       done();
     })
  });







});
